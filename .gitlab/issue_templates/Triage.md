[Triage process](https://about.gitlab.com/handbook/engineering/development/enablement/distribution/triage.html)

Links:

- [Omnibus issues to triage](https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues?assignee_id=None&milestone_title=None&not%5Blabel_name%5D%5B%5D=For+Scheduling&not%5Blabel_name%5D%5B%5D=awaiting+feedback&not%5Blabel_name%5D%5B%5D=needs+investigation&scope=all&sort=created_date&state=opened)
- [Charts issues to triage](https://gitlab.com/gitlab-org/charts/gitlab/-/issues?assignee_id=None&milestone_title=None&not%5Blabel_name%5D%5B%5D=For+Scheduling&not%5Blabel_name%5D%5B%5D=awaiting+feedback&not%5Blabel_name%5D%5B%5D=needs+investigation&scope=all&sort=created_date&state=opened)
- [Operator issues to triage](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/-/issues?assignee_id=None&milestone_title=None&not%5Blabel_name%5D%5B%5D=For+Scheduling&not%5Blabel_name%5D%5B%5D=awaiting+feedback&not%5Blabel_name%5D%5B%5D=needs+investigation&scope=all&sort=created_date&state=opened)
- Omnibus Issue list [`awaiting feedback`, sorted by `Last Updated`](https://gitlab.com/gitlab-org/omnibus-gitlab/issues?assignee_id=None&label_name%5B%5D=awaiting+feedback&milestone_title=No+Milestone&page=3&scope=all&sort=updated_desc&state=opened) (start from last page)
- Chart Issue list [`awaiting feedback`, sorted by `Last Updated`](https://gitlab.com/gitlab-org/charts/gitlab/issues?label_name%5B%5D=awaiting+feedback&milestone_title=None&scope=all&state=opened) (start from last page)
- Operator Issue list [`awaiting feedback`, sorted by `Last Updated`](https://gitlab.com/gitlab-org/cloud-native/gitlab-operator/issues?label_name%5B%5D=awaiting+feedback&milestone_title=None&scope=all&state=opened) (start from last page)
- [Pipeline failures spreadsheet](https://docs.google.com/spreadsheets/d/128D3H_8MDu8YEXm691Pmjpq8tgZRy00GtY61z72ze-E/edit#gid=0)
- ~Triage [issue history](https://gitlab.com/gitlab-org/distribution/team-tasks/issues?scope=all&utf8=%E2%9C%93&state=all&label_name%5B%5D=Triage)

/label ~Triage ~"group::distribution" ~"devops::enablement" ~"section::enablement"
/due next friday
